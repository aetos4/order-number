package cz.aetos.monetahw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonetahwApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonetahwApplication.class, args);
	}

}
