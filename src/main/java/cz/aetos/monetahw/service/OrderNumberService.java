package cz.aetos.monetahw.service;

import cz.aetos.monetahw.db.OrderNumberRepository;
import cz.aetos.monetahw.db.model.OrderNumberEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

@AllArgsConstructor
@Service
public class OrderNumberService {

    private final OrderNumberRepository repository;

    @Transactional
    public synchronized OrderNumberEntity generateOrderNumber() {
        Long order = repository.getMaxFromOrder();
        order = order == null ? 0L : order + 1;

        return repository.save(
                OrderNumberEntity.builder()
                        .order(order)
                        .dateTime(ZonedDateTime.now()).build());
    }

    @Transactional(readOnly = true)
    public OrderNumberEntity getCurrent() {
        Long order = repository.getMinFromOrder();
        return repository.findByOrder(order);
    }

    @Transactional
    public synchronized void deleteFirst() {
        Long order = repository.getMinFromOrder();
        repository.deleteByOrder(order);

        List<OrderNumberEntity> entities = repository.findAll();
        entities.forEach(entity -> entity.setOrder(entity.getOrder()-1));
        repository.saveAll(entities);
    }
}
