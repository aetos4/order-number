package cz.aetos.monetahw.controller.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OrderNumberDTO {

    private Long orderNumber;
    private String dateTime;
    private Long order;

}
