package cz.aetos.monetahw.controller.model;

import cz.aetos.monetahw.db.model.OrderNumberEntity;

import java.time.format.DateTimeFormatter;

public class OrderNumberConvertor {

    private static final DateTimeFormatter datetimeFormatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public static OrderNumberDTO convert(OrderNumberEntity entity) {
        if (entity == null) return null;
        return OrderNumberDTO.builder()
                .orderNumber(entity.getOrderNumber())
                .dateTime(datetimeFormatter.format(entity.getDateTime()))
                .order(entity.getOrder()).build();
    }

}
