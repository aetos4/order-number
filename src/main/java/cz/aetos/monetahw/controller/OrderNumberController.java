package cz.aetos.monetahw.controller;

import cz.aetos.monetahw.controller.model.OrderNumberDTO;
import cz.aetos.monetahw.service.OrderNumberService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static cz.aetos.monetahw.controller.model.OrderNumberConvertor.convert;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/order-number")
public class OrderNumberController {

    private final OrderNumberService orderNumberService;

    @PostMapping
    public ResponseEntity<OrderNumberDTO> generate() {
        log.info("Generating order number");
        return ResponseEntity.ok(convert(orderNumberService.generateOrderNumber()));
    }

    @GetMapping
    public ResponseEntity<OrderNumberDTO> get() {
        log.info("Getting order number");
        return ResponseEntity.ok(convert(orderNumberService.getCurrent()));
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteLast() {
        log.info("Deleting last order number");
        orderNumberService.deleteFirst();
        return ResponseEntity.noContent().build();
    }


}
