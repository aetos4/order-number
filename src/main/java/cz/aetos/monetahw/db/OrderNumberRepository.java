package cz.aetos.monetahw.db;

import cz.aetos.monetahw.db.model.OrderNumberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderNumberRepository extends JpaRepository<OrderNumberEntity, Long> {

    @Query("select max(s.order) from OrderNumberEntity s")
    Long getMaxFromOrder();

    @Query("select min(s.order) from OrderNumberEntity s")
    Long getMinFromOrder();

    OrderNumberEntity findByOrder(Long orderNumber);

    void deleteByOrder(Long orderNumber);

}
