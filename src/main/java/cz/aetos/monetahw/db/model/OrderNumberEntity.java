package cz.aetos.monetahw.db.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "order_number", indexes = @Index(columnList = "order_seq"))
public class OrderNumberEntity {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "order_number")
    private Long orderNumber;

    @Column(name = "date_time", nullable = false)
    private ZonedDateTime dateTime;

    @Column(name="order_seq", nullable = false)
    private Long order;

}
