package cz.aetos.monetahw;

import cz.aetos.monetahw.db.model.OrderNumberEntity;
import cz.aetos.monetahw.service.OrderNumberService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class MonetahwApplicationTests {

	@Autowired
	OrderNumberService service;

	@Test
	void basePositiveTest() {
		Assert.isNull(service.getCurrent(), "Empty queue, current item should be null");
		service.generateOrderNumber();
		service.generateOrderNumber();
		OrderNumberEntity entity = service.generateOrderNumber();
		Assert.notNull(entity, "Entity shouldn't be null");
		Assert.isTrue(entity.getOrder() == 2, "Entity should have right order");
		service.deleteFirst();
		Assert.isTrue(service.getCurrent().getOrder() == 0, "Current has always zero order");
		service.deleteFirst();
		service.deleteFirst();
		Assert.isNull(service.getCurrent(), "Empty queue, current item should be null");
	}

}
